package com.infostretch.utility;





import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;


public class myutility extends WebDriverBaseTestPage<WebDriverTestPage>  {
	
	public static Properties pr;
	public static Actions act;
	
	protected void openPage(PageLocator locator, Object... args) 
	{
		
		
	}
	public static void verifyelementtext(QAFWebElement ele,String eletext) 
	{
		
		  Validator.verifyTrue(true,ele.getText(),eletext);
	}
	
	public   void navigatebacck() 
	{
		driver.navigate().back();
	}
	
	public static void scrollIntoAxis(int x,int y) 
	{
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("window.scrollTo(" + x + ","+ y +");"); 	
		
	} 
	
	

	public static void javaScriptscrolltillelementk(QAFWebElement ele1) 
	{
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView();", ele1);
	}
	
	
	public static void clickusingjavascript(QAFWebElement ele)
	{
		
      JavascriptExecutor executor = (JavascriptExecutor)new WebDriverTestBase().getDriver();;
	executor.executeScript("arguments[0].click();",ele);
	}
	
	public static void clickusingaction(QAFWebElement ele)
	{
	
	Actions o = new Actions(new WebDriverTestBase().getDriver());
	o.moveToElement(ele).click();
	
	}
	
	public  void selectoption(String menu,String submenu)
	{ 
			QAFExtendedWebElement catogoryname = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getPropertyValue("home.menuList.link"), menu));
			myutility.clickusingjavascript(catogoryname);
		    QAFExtendedWebElement subcatogory = new QAFExtendedWebElement(String.format(
		    ConfigurationManager.getBundle().getPropertyValue("home.subMenuList.link"), submenu));
		    subcatogory.waitForPresent(5000);
		    myutility.clickusingjavascript(subcatogory);
		   
	}
	
	
	
	}
	
 

			
				
				
				
	
	



