package com.infostretch.pages;

import com.infostretch.utility.myutility;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class TravelReqPage {
	public static QAFExtendedWebElement ele;

	public static void checkapprovebtnisenable() {
		ele = new QAFExtendedWebElement("TravelRequest.btn.Approvebtn");
		myutility.javaScriptscrolltillelementk(ele);
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.waitForVisible("TravelRequest.btn.Approvebtn");
		CommonStep.verifyVisible("TravelRequest.btn.Approvebtn");
		CommonStep.verifyEnabled("TravelRequest.btn.Approvebtn");
		Validator.verifyTrue(true, ele.getText(), "Approve");
	}

	public static void checksubmitbtnisenable() {

		ele = new QAFExtendedWebElement("TravelRequest.btn.Rejectbtn");
		CommonStep.waitForVisible("TravelRequest.btn.Rejectbtn");
		CommonStep.verifyVisible("TravelRequest.btn.Rejectbtn");
		CommonStep.verifyEnabled("TravelRequest.btn.Rejectbtn");
		Validator.verifyTrue(true, ele.getText(), " Reject");

	}

	public static void checkcancelbtnisenable() {

		ele = new QAFExtendedWebElement("TravelRequest.btn.cancelbtn");
		CommonStep.waitForVisible("TravelRequest.btn.cancelbtn");
		CommonStep.verifyVisible("TravelRequest.btn.cancelbtn");
		CommonStep.verifyEnabled("TravelRequest.btn.cancelbtn");
		Validator.verifyTrue(true, ele.getText(), " Back");

	}
}