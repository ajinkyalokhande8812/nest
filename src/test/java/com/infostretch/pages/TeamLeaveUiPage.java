package com.infostretch.pages;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class TeamLeaveUiPage extends WebDriverTestBase {

	@FindBy(locator = "TeamLeavelist.main.parent")
	private List<QAFWebElement> parentMenu;

	public List<QAFWebElement> getparent() {

		return parentMenu;
	}

	public void verifyData(String data) {
		String arr[] = data.split(",");
		List<String> list = Arrays.asList(arr);
		for (int i = 0; i < list.size(); i++) {
			Validator.verifyThat(new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("TeamLeavelist.main.parent"), list.get(i)))
							.isPresent(),
					Matchers.equalTo(true));

		}
	}

}
