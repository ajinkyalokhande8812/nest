package com.infostretch.pages;

import com.infostretch.utility.myutility;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class ReimbursementExp {

	public void selectoption(String option) {
		QAFExtendedWebElement myoption = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getPropertyValue("reimbursement.optiondynamic.selectexpensecatogory"),
				option));

		myutility.javaScriptscrolltillelementk(myoption);
	}

	public void selectemployee(String selectdata) {

		QAFExtendedWebElement myoption = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("reimburesment.datatable.emploeedynamic"), selectdata));
		myutility.clickusingaction(myoption);
	}

}
