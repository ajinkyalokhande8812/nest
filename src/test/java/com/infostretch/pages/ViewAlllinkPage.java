package com.infostretch.pages;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class ViewAlllinkPage {

	public static void check_view_all_of_leavereq() {
		CommonStep.waitForNotVisible("homepage.image.loader");

		CommonStep.waitForPresent("ViewAllLink.lnk.viewall", 60);
		CommonStep.click("ViewAllLink.lnk.viewall");
		CommonStep.waitForNotVisible("homepage.image.loader");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("ViewAllLink.txt.teamleavetext");
		ele.isPresent();
		Validator.assertTrue(true, ele.getText(), "Team Leave List");
		CommonStep.click("ViewAllLink.btn.HomeButton");
		CommonStep.waitForNotVisible("homepage.image.loader");

	}

	public static void check_view_all_of_expensereq() {
		CommonStep.click("ViewAllLink.btn.expenserequest");
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.waitForPresent("ViewAllLink.lnk.viewall", 60);
		CommonStep.click("ViewAllLink.lnk.viewall");
		CommonStep.waitForNotVisible("homepage.image.loader");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("ViewallLink.txt.TeamReumbsList");
		ele.isPresent();
		Validator.assertTrue(true, ele.getText(), "Team Reimbursement List");
		CommonStep.click("ViewAllLink.btn.HomeButton");
		CommonStep.waitForNotVisible("homepage.image.loader");

	}

	public static void check_view_all_of_TravelRequest() {
		CommonStep.click("ViewAllLink.btn.travelrequest");
		CommonStep.waitForNotVisible("homepage.image.loader");
		// CommonStep.verifySelected("ViewAllLink.btn.leaveRequest");

		CommonStep.waitForPresent("ViewAllLink.lnk.viewall", 60);
		CommonStep.click("ViewAllLink.lnk.viewall");
		CommonStep.waitForNotVisible("homepage.image.loader");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("ViewallLink.txt.Travelrequest");
		ele.isPresent();
		Validator.assertTrue(true, ele.getText(), "Travel Requests");
	}

}
	
	

