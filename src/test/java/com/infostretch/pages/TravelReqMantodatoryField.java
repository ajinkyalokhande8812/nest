package com.infostretch.pages;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class TravelReqMantodatoryField {

	@FindBy(locator = "addTravelReq.allfieldmandotry.second")
	private List<QAFWebElement> allfieldvalue;

	public List<QAFWebElement> alluifields() {

		return allfieldvalue;
	}

	@FindBy(locator = "addtravelrequest.btn.astrikmark")
	private List<QAFWebElement> astrimarkfield;

	public List<QAFWebElement> astrikfield() {

		return astrimarkfield;
	}

	public void verifyData(String data) {
		String arr[] = data.split(",");
		List<String> list = Arrays.asList(arr);
		for (int i = 0; i < list.size(); i++) {
			Validator.verifyThat(new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString("addTravelReq.allfieldmandotry.second"), list.get(i)))
							.isPresent(),
					Matchers.equalTo(true));
		}

	}
	 
	
	
		
	}


	
