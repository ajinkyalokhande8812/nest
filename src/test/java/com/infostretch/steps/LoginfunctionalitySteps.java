package com.infostretch.steps;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Validator;

public class LoginfunctionalitySteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {

	}

	@QAFTestStep(description = "Open the URL")
	public void Open_the_URL() {

		driver.get("/");
		CommonStep.waitForNotVisible("homepage.image.loader");
		driver.manage().window().maximize();

	}

	@QAFTestStep(description = "user Enter valid username as {0} and user Enter valid password as {1}")
	public static void login_with_username_password(String uname, String pass) {

		CommonStep.sendKeys(uname, "loginpage.input.username");
		CommonStep.sendKeys(pass, "loginpage.input.password");
		;

	}

	@QAFTestStep(description = "user click on login button")
	public static void user_click_on_login_button() {

		CommonStep.click("loginpage.btn.loginbt");
		CommonStep.waitForNotVisible("homepage.image.loader");
	}

	@QAFTestStep(description = "Error message should be display as {0}")
	public static void Error_message_should_be_displayed(String msg) {
		String errormsg = CommonStep.getText("loginpage.txt.errormsg");
		Validator.verifyThat("this erro mesaage should display", errormsg, Matchers.equalToIgnoringCase(msg));
	}

	@QAFTestStep(description = "user should login into home page")
	public void user_should_login_into_home_page() {
		Validator.verifyThat("verification for home page title", driver.getTitle(),
				Matchers.equalToIgnoringCase("Infostretch NEST"));

	}

	@QAFTestStep(description = "click on logout image button")
	public static void click_on_logout_button() {
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("homepage.btn.logout");

	}

	@QAFTestStep(description = "user should redirect to sign up page")
	public void user_should_redirect_to_sign_up_page() {
		Validator.verifyThat("verification for home page title", driver.getTitle(),
				Matchers.equalToIgnoringCase("Infostretch NEST"));

	}

}
	



