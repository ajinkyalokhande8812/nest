package com.infostretch.steps;

import java.util.Arrays;
import java.util.List;
import org.hamcrest.Matchers;
import org.openqa.selenium.Point;
import com.infostretch.utility.myutility;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class PublisherStepAll extends WebDriverBaseTestPage<WebDriverTestPage> {
	QAFExtendedWebElement ele;
	myutility m = new myutility();

	@FindBy(locator = "publisher.txt.breadcumloc")
	private List<QAFWebElement> breadcum;

	public List<QAFWebElement> getBreadcum() {
		return breadcum;
	}

	@FindBy(locator = "publisher.txt.alluifield")
	private List<QAFWebElement> alluifield;

	public List<QAFWebElement> getAlluifield() {
		return alluifield;
	}

	@FindBy(locator = "publisher.dropdown.locationdropdown")
	private List<QAFWebElement> dropdwnlocation;

	public List<QAFWebElement> getDropdwnlocation() {
		return dropdwnlocation;
	}

	@FindBy(locator = "publisher.dropdown.selectoptnallvalue")
	private List<QAFWebElement> catogoryallvalue;

	public List<QAFWebElement> getcatogory() {
		return catogoryallvalue;
	}

	@QAFTestStep(description = "user click on Go to Publisher")
	public void user_click_on_Go_to_Publisher() {
		QAFExtendedWebElement ele = new QAFExtendedWebElement("publisher.btn.menuoption");
		Point p = ele.getLocation();
		int xcord = p.getX();
		int ycord = p.getY();
		myutility.scrollIntoAxis(xcord, ycord);
		m.selectoption("Publisher", "My Posts");
	}

	@QAFTestStep(description = "user click on create button")
	public void user_click_on_create_button() {

		CommonStep.click("publisher.btn.create");

		QAFExtendedWebElement ele = new QAFExtendedWebElement("publisher.txt.Mypostfield");
		myutility.javaScriptscrolltillelementk(ele);

	}

	@QAFTestStep(description = "user should able to see {0} on page")
	public void breadcumgui(String data) {

		String arr[] = data.split(",");
		List<String> list = Arrays.asList(arr);
		for (int i = 0; i < list.size(); i++) {
			Validator.verifyThat(new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("publisher.txt.breadcumloc"), list.get(i)))
							.isPresent(),
					Matchers.equalTo(true));

		}

	}

	@QAFTestStep(description = "user should able to view allfield {0} on page")
	public void allui_field(String data) {
		String arr1[] = data.split(",");
		List<String> list = Arrays.asList(arr1);

		for (int i = 0; i < alluifield.size(); i++) {

			Validator.verifyThat(new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString("publisher.txt.alluifield"), list.get(i)))
							.isPresent(),
					Matchers.equalTo(true));

		}

	}

	@QAFTestStep(description = "user should able to view dropdwon with all option {0} on page")
	public void dropdown_location(String data) {
		String arr2[] = data.split(",");
		List<String> list1 = Arrays.asList(arr2);
		CommonStep.click("publisher.btn.clickondropdown");
		ele = new QAFExtendedWebElement("publisher.txt.Mypostfield");
		myutility.javaScriptscrolltillelementk(ele);

		List<QAFWebElement> ele = driver.findElements("publisher.dropdown.locationdropdown");

		for (int i = 0; i < ele.size() - 1; i++) {
			Validator.verifyThat(new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString("publisher.dropdown.locationdropdown"), list1.get(i)))
							.isPresent(),
					Matchers.equalTo(true));

		}
	}

	@QAFTestStep(description = "user should able view catogory dropdown value {0} on page")
	public void dropdown_catogory(String data) {

		String[] arr4 = data.split(",");

		CommonStep.click("publisher.dropdown.clickselectcatogory");

		List<QAFWebElement> ele = driver.findElements("publisher.dropdown.selectoptnallvalue");

		for (int i = 0; i < ele.size(); i++) {

			Validator.verifyTrue(ele.get(i).getText().trim().contains(arr4[i]), "Fail", "pass");
		}
	}

	@QAFTestStep(description = "user should able to see Buttons Submit , Back button on screen")
	public void submit_back_button() {
		ele = new QAFExtendedWebElement("publisher.btn.submit");
		Validator.verifyTrue(ele.isEnabled() && ele.isPresent(), "submit button  not present and disable",
				"submit button is present and enable");
		ele = new QAFExtendedWebElement("publisher.btn.cancel");
		Validator.verifyTrue(ele.isEnabled() && ele.isPresent(), "button is not present and disable",
				" cancel button is present and enable");
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

}


