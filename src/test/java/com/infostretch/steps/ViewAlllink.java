package com.infostretch.steps;

import com.infostretch.pages.ViewAlllinkPage;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class ViewAlllink extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@QAFTestStep(description = "user should be able to navigate All request tab")
	public void view_all_link_with_all_req_tab() throws InterruptedException {

		ViewAlllinkPage.check_view_all_of_leavereq();
		ViewAlllinkPage.check_view_all_of_expensereq();
		ViewAlllinkPage.check_view_all_of_TravelRequest();
	}

}
