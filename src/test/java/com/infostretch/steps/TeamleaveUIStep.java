package com.infostretch.steps;

import com.infostretch.pages.TeamLeaveUiPage;
import com.infostretch.utility.myutility;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class TeamleaveUIStep {

	TeamLeaveUiPage teamleave = new TeamLeaveUiPage();

	@QAFTestStep(description = "Navigate to Leave module")
	public void Navigate_to_Leave_module() throws InterruptedException {
		QAFExtendedWebElement ele1 = new QAFExtendedWebElement("TeamLeaveList.btn.leaveoption");
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("TeamLeaveList.btn.mainmenu");
		myutility.clickusingjavascript(ele1);

		CommonStep.waitForVisible("TeamleaveList.btn.myleavelist");
		CommonStep.click("TeamleaveList.btn.myleavelist");
		CommonStep.waitForNotVisible("homepage.image.loader");

	}

	@QAFTestStep(description = "user should see {0} on page")
	public void user_should_see_following_fields_on_page(String data) {
		teamleave.verifyData(data);

	}

}