package com.infostretch.steps;

import com.infostretch.pages.TravelReqMantodatoryField;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class AddTravelRequestUI extends WebDriverBaseTestPage<WebDriverTestPage> {
	TravelReqMantodatoryField t = new TravelReqMantodatoryField();

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@QAFTestStep(description = "user click on {0} button")
	public void user_click_on_view_all_button() throws InterruptedException {

		CommonStep.click("addtravelrequest.btn.viewalllinkt");
		CommonStep.waitForNotVisible("homepage.image.loader");

	}

	@QAFTestStep(description = "user click on new travel request button")
	public void user_click_on_add_new_travel_req() throws InterruptedException {

		CommonStep.waitForVisible("addtravelrequest.btn.newtravelreq");
		CommonStep.click("addtravelrequest.btn.newtravelreq");
		Thread.sleep(9000);

	}

	@QAFTestStep(description = "User should see following fields {0} on page")
	public void user_should_see_following_fields_on_page(String data) {
		t.verifyData(data);

	}

}