package com.infostretch.steps;

import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;

import com.infostretch.pages.ReimbursementExp;
import com.infostretch.utility.myutility;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class ReimbursementExpanseReq {

	myutility ut1 = new myutility();
	ReimbursementExp rsmb = new ReimbursementExp();
	public static QAFExtendedWebElement ele;

	@QAFTestStep(description = "user click on menu button")
	public void user_click_on_menu_button() {
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("maintoolbutton.btn.main");

	}

	@QAFTestStep(description = "user should Navigate to Expense Reimbursement and select My Expense List")
	public void user_should_Navigate_to_Expense_Reimbursement_and_select_My_Expense_List() {

		ut1.selectoption("Reimbursement", "My Expense List");
		CommonStep.waitForNotVisible("homepage.image.loader");

	}

	@QAFTestStep(description = "user click on newexpense button")
	public void user_click_on_newexpense_button() {
		CommonStep.click("Reimbursement.btn.newtravelreq");
		CommonStep.waitForNotVisible("homepage.image.loader");
	}

	@QAFTestStep(description = "user Fill the required valid details and click on Submit Button")
	public void Fill_the_required_details_and_click_on_Submit_Button() {
		CommonStep.sendKeys("mobile bill", "Reimbursement.input.title");
		ele = new QAFExtendedWebElement("Reimbursement.input.datetable");
		ele.clear();
		ele.sendKeys("12-Jul-2018");
		CommonStep.click("reimbursement.input.project");
		CommonStep.waitForVisible("reimbursement.option.selectproject");
		CommonStep.click("reimbursement.option.selectproject");
		ele = new QAFExtendedWebElement("reimbursement.inpt.selectexpensecatogory2");
		ele.click();
		ele.sendKeys(Keys.DOWN);
		ele.sendKeys(Keys.DOWN);
		ele.sendKeys(Keys.ENTER);
		ele = new QAFExtendedWebElement("reimbursement.input.enteramount");
		ele.sendKeys("7890");
		ele = new QAFExtendedWebElement("reimbursement.input.billno");
		ele.sendKeys("1112");
		ele = new QAFExtendedWebElement("reimbursment.btn.attachment");
		CommonStep.click("reimburesment.btn.submit");
		CommonStep.waitForNotVisible("homepage.image.loader");

	}

	@QAFTestStep(description = "There should be a success message on successful submission of request")
	public void There_should_be_a_success_message_on_successful_submission_of_request() {
		ele = new QAFExtendedWebElement("reimburesment.msg.sucessfulmsg");
		ele.waitForVisible(50000);
		Validator.verifyThat(ele.getText(), Matchers.equalToIgnoringCase("Expense submitted successfully."));
	}

	@QAFTestStep(description = "user click on logout button")
	public void user_click_on_logout_button() {
		ele = new QAFExtendedWebElement("reimburesment.btn.logout");

		myutility.clickusingjavascript(ele);
	}

	@QAFTestStep(description = "click on toolbar button")
	public void click_on_toolbar_button() {
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("maintoolbutton.btn.main");
	}

	@QAFTestStep(description = "Navigate to Expense Reimbursement-Team Reimbursement List")
	public void Navigate_to_Expense_Reimbursement_Team_Reimbursement_List() {

		ut1.selectoption("Reimbursement", "Teams Reimbursement List");

	}

	@QAFTestStep(description = "Click on raised request by employee")
	public void Click_on_raised_request_by_employee() {
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("reimburesment.btn.fristemptitle");

	}

	@QAFTestStep(description = "approve the raised request and observe")
	public void approve_the_raised_request_and_observe() throws InterruptedException {
		ele = new QAFExtendedWebElement("reimburesment.btn.approvebutton");
		Point p = ele.getLocation();
		int xcord = p.getX();
		int ycord = p.getY();
		System.out.println("my x axis co-ordinate is" + xcord);
		System.out.println("my x axis co-ordinate is" + ycord);
		myutility.scrollIntoAxis(xcord, ycord);
		CommonStep.click("reimburesment.txt.textarea");

		CommonStep.sendKeys("hiiiii", "reimburesment.txt.textarea");
		Validator.verifyTrue(ele.isEnabled(), "disable", " Apporve button enable");
		ele.click();

	}

	@QAFTestStep(description = "Manager will be able to approve the raised request")
	public void Manager_will_be_able_to_approve_the_raised_request() {
		ele = new QAFExtendedWebElement("reimburesment.msg.sucessful");
		CommonStep.waitForNotVisible("homepage.image.loader");
		ele.waitForVisible(8000);
		Validator.verifyThat(ele.getText(), Matchers.equalToIgnoringCase("Request approved successfully"));
	}

	@QAFTestStep(description = "Fill the invalid details and click on Submit Button")
	public void Fill_the_invalid_details_and_click_on_Submit_Button() {

		CommonStep.sendKeys("mobile bill", "Reimbursement.input.title");
		ele = new QAFExtendedWebElement("Reimbursement.input.datetable");
		ele.clear();
		ele.sendKeys("abcd-1222-2018");

		CommonStep.click("reimbursement.input.project");
		CommonStep.waitForVisible("reimbursement.option.selectproject");
		CommonStep.click("reimbursement.option.selectproject");
		ele = new QAFExtendedWebElement("reimbursement.inpt.selectexpensecatogory2");
		ele.click();
		ele.sendKeys(Keys.DOWN);
		ele.sendKeys(Keys.DOWN);
		ele.sendKeys(Keys.ENTER);
		ele = new QAFExtendedWebElement("reimbursement.input.enteramount");
		ele.sendKeys("abcd");
		ele = new QAFExtendedWebElement("reimbursement.input.billno");
		ele.sendKeys("abcd");
		ele = new QAFExtendedWebElement("reimbursment.btn.attachment");
		CommonStep.click("reimburesment.btn.submit");
		CommonStep.waitForNotVisible("homepage.image.loader");
	}

}

