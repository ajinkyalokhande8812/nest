package com.infostretch.steps;

import com.infostretch.pages.TravelReqPage;
import com.infostretch.utility.myutility;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class TravelReqSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@QAFTestStep(description = "user Click on Manager view Tab")
	public static void user_Click_on_Manager_view_Tab() {

		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("TravelRequest.btn.managerview");

	}

	@QAFTestStep(description = "click on Travel request tab")
	public static void click_on_Travel_request_tab() {
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("TravelRequest.btn.travelrequestbtn");
		CommonStep.waitForNotVisible("homepage.image.loader");

	}

	@QAFTestStep(description = "click on view button")
	public void click_on_view_button() {
		CommonStep.waitForVisible("TravelRequest.btn.viewimgbtn", 40);

		CommonStep.waitForNotVisible("homepage.image.loader");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("TravelRequest.btn.viewimgbtn");
		myutility.clickusingjavascript(ele);

	}

	@QAFTestStep(description = "3 buttons approve,submit,cancel should be enable")
	public static void user_can_able_to_click_3_buttons_approve_submit_cancel() {

		TravelReqPage.checkapprovebtnisenable();
		TravelReqPage.checkcancelbtnisenable();
		TravelReqPage.checksubmitbtnisenable();

	}

	@QAFTestStep(description = "click on view al link")
	public static void click_on_view_al_link() {
		CommonStep.waitForNotVisible("homepage.image.loader");
		CommonStep.click("TravelRequest.lnk.viewalllink1");

	}

}
	
	


